/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



//import org.apache.commons.lang3.ArrayUtils;


/**
 *
 * @author Emma
 */
public class Main {
    public static void main(String[] arg){
    
        //Assumed values
        int nPeriods = 2;
        double gamma = 0.16666;
        double chargingMax = 100; //Only testing, normally 20
        double dischargingMax = 100; //Only testing, normally 20
        double pi = 1;
        double alpha = 0.4;
        double etaCharging = 0.9;
        double etaDischarging = 1; //assume for simplicity that there is no loss when discharging
        double rho = 23.4;
        double stateMax = 60;
        double stateMin = 0.2*stateMax;
        double p = 0.8;        
        double engineMax = 30;
        
        //Define the number of states in discretisized battery state space.
        int numberOfStates = 3;
        
        //Driving data
        double d[] = {20,0};
        //Assugn values to delta from the driving data
        double delta[] = new double[nPeriods];
        for(int t=1; t <= nPeriods; t++){
            if(d[t-1]>0){
                delta[t-1] = 1;
            }else{
                delta[t-1] = 0;
            }
        }
        
        //Create an instance of the problem
        OptimalChargingOneProblem ocop = new OptimalChargingOneProblem(nPeriods, p, pi, d, delta, etaCharging, etaDischarging, rho, gamma, alpha, stateMin, stateMax, chargingMax,
        dischargingMax, engineMax);
        
        //Set the start and ending state
        double l_T = stateMax;
        double l_0 = l_T;

        
        //Discretizise the battery state
        double length = (stateMax - stateMin)/(numberOfStates - 1);
        double states[] = new double[numberOfStates];
        for(int i=1 ; i <= numberOfStates ; i++){
            if(i > 1){
                states[i-1] = stateMin + (i-1)*length;
                
            }else{
                states[i-1] = stateMin;
            }        
        }
              
        //Defining objects to hold costs and values for charging, discharging and engine use.
        double J[] = new double[numberOfStates];
        double u_plus[][][] = new double[numberOfStates][numberOfStates][ocop.getnPeriods()];
        double u_minus[][][] = new double[numberOfStates][numberOfStates][ocop.getnPeriods()];
        double w[][][] = new double[numberOfStates][numberOfStates][ocop.getnPeriods()];
        double K[]=new double[numberOfStates]; 
        
        for(int t=ocop.getnPointsInTime() ; t >= 1 ; t--){ //Start at the last point in time and move backwards.
                           
            if(t < ocop.getnPointsInTime()){ //The last, second to last and first point in time gets special treatment.
                if(t< ocop.getnPointsInTime() -1){
                    if(t < 2){
                        
                         //In the tirst period the state is fexed to l_0, so I do not loop over s.
                            for(int c=1 ; c <= numberOfStates ; c++){ //I loop over all the possible states at time 1.
                                
                                //Calculation of charging, discharging and engine use.
                                u_plus[0][c-1][t-1] = Math.max((1-ocop.getDelta()[t-1])*(states[c-1] - l_0)*Math.pow(ocop.getEtaCharging(), -1), 0);
                                u_minus[0][c-1][t-1] = Math.max((1-ocop.getDelta()[t-1])*(l_0 - states[c-1]), 0);
                                w[0][c-1][t-1] = ocop.getDelta()[t-1]*(states[c-1] - l_0 + d[t-1])*Math.pow(ocop.getRho(), -1);
                                
                                //Calculationg the cost of going to state c at time 1.
                                if(u_plus[0][c-1][t-1] <= ocop.getChargingMax() && u_minus[0][c-1][t-1] <= ocop.getDischargingMax() 
                                        && 0 <= w[0][c-1][t-1] && w[0][c-1][t-1] <= ocop.getEngineMax()){
                                    
                                    K[c-1] = p*(u_plus[0][c-1][t-1] - ocop.getEtaDischarging()*u_minus[0][c-1][t-1]) + pi*w[0][c-1][t-1] + alpha*u_minus[0][c-1][t-1] + J[c-1];
                                    
                                }else{ //If the limits for charging, discharging and enginge use is not met, the cost is set to infinity,
                                       //so that it will never be the minimum over all the K's.
                                    
                                    K[c-1] = Double.POSITIVE_INFINITY;
                                }    
                            }
                            //Set J[0] to be the minimum of the costs of all states at time 1
                            //J only has one value since the state at time 0 is fixed
                            J[0] = K[0];
                            for(int c=1 ; c < numberOfStates ; c++){
                                if(K[c] < J[0]){
                                    J[0] = K[c];
                                }                                
                            }
                            System.out.println("Total cost: J_"+t+"(l_0)="+J[0]);
                                        
                    }else{ //In the rest of the points in time I loop over all states in two loops
                        
                        for(int s=1 ; s <= numberOfStates ; s++){
                            for(int c=1 ; c <= numberOfStates ; c++){
                                //Calculate the charging, discharging and use of engine for each combination of states.
                                u_plus[s-1][c-1][t-1] = Math.max((1-ocop.getDelta()[t-1])*(states[c-1] - states[s-1])*Math.pow(ocop.getEtaCharging(),-1), 0);
                                u_minus[s-1][c-1][t-1] = Math.max((1-ocop.getDelta()[t-1])*(states[s-1] - states[c-1]), 0);
                                w[s-1][c-1][t-1] = ocop.getDelta()[t-1]*(states[c-1] - states[s-1] + d[t-1])*Math.pow(ocop.getRho(), -1);
                                
                                //To rule out combination af states that is not feasible according to the capacities, I set the cost to infinity.
                                if(u_plus[s-1][c-1][t-1] <= ocop.getChargingMax() && u_minus[s-1][c-1][t-1] <= ocop.getDischargingMax() 
                                        && 0 <= w[s-1][c-1][t-1] && w[s-1][c-1][t-1] <= ocop.getEngineMax()){
                                    K[c-1] = p*(u_plus[s-1][c-1][t-1] - ocop.getEtaDischarging()*u_minus[s-1][c-1][t-1]) + pi*w[s-1][c-1][t-1] + alpha*u_minus[s-1][c-1][t-1] + J[c-1];                                
                                }else{
                                    K[c-1] = Double.POSITIVE_INFINITY;
                                }
                            
                            }
                            //Way to find the minimum cost (possibly find faster way)
                            J[s-1] = K[0];
                            for(int c=1 ; c < numberOfStates ; c++){
                                if(K[c] < J[s-1]){
                                    J[s-1] = K[c];
                                }
                            }
                            System.out.println("J_"+t+"("+states[s-1]+")="+J[s-1]);
                    
                        }
                    }
                }else{ //At time T-1 we do not need to minimize over battery states in time T, since it has a known value.
                    for(int s=1 ; s <= numberOfStates ; s++){
                        
                        //I do not loop over c, since there is only one possible state at time T, l_T.                        
                            
                        //Calculating charging, discharging and engine use.
                        u_plus[s-1][0][t-1] = Math.max((1-ocop.getDelta()[t-1])*(l_T - states[s-1])*Math.pow(ocop.getEtaCharging(), -1), 0);
                        u_minus[s-1][0][t-1] = Math.max((1-ocop.getDelta()[t-1])*(states[s-1]-l_T), 0);
                        w[s-1][0][t-1] = ocop.getDelta()[t-1]*(l_T-states[s-1]+d[t-1])*Math.pow(ocop.getRho(),-1);

                        /*
                        System.out.println("u_plus_"+t+"("+states[s-1]+")="+u_plus[s-1][0][t-1]);
                        System.out.println("u_minus_"+t+"("+states[s-1]+")="+u_minus[s-1][0][t-1]);
                        System.out.println("w_"+t+"("+states[s-1]+")="+w[s-1][0][t-1]);
                        */
                        
                        if(u_plus[s-1][0][t-1] <= ocop.getChargingMax() && u_minus[s-1][0][t-1] <= ocop.getDischargingMax() 
                                    && 0 <= w[s-1][0][t-1] && w[s-1][0][t-1] <= ocop.getEngineMax()){                               
                        K[s-1] = p*(u_plus[s-1][0][t-1]-ocop.getEtaDischarging()*u_minus[s-1][0][t-1]) + pi*w[s-1][0][t-1] + alpha*u_minus[s-1][0][t-1] + J[0];
                        }else{
                            K[s-1] = Double.POSITIVE_INFINITY;
                        }                                                
                    }
                    //Set J to be the minimum cost
                    for(int s=1 ; s <= numberOfStates ; s++){
                        J[s-1] = K[s-1];
                        System.out.println("J_"+t+"("+states[s-1]+")="+J[s-1]);
                    }
                }
            }else{ //At the time T the cost is 0.                                
                    J[0]=0;
                    System.out.println("J_"+t+"(l_T)="+J[0]);               
            }
            
            
                
        
        }
        
        
    //The total cost is J[0], but what is the solution?
    
   
     
    
    
    
    
    
    
        
        
        
        
    
    }   
}
